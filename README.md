# Vetvite Traefik

> Vetvite Trafik is an instance of Traefik for the reverse proxy.

## Requirements
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/) (19 <=)

## How to setup ?
Remember to change the domain in the Traefik label in **docker-compose.yml**.

Add your certificate to the ssl folder :
- ssl/yokode.dev.cert
- ssl/yokode.dev.key

Launch the containers :
```sh
docker-compose up -d
```


## Authors

| <a href="https://gitlab.com/christophele" target="_blank">**Christophe LE**</a> | <a href="https://gitlab.com/kiliandiogo" target="_blank">**Kilian DIOGO**</a> | <a href="https://gitlab.com/nolway" target="_blank">**Alexis FAIZEAU**</a> | <a href="https://gitlab.com/kamISKRANE" target="_blank">**Kamel ISKRANE**</a> | <a href="https://gitlab.com/BaptisteVasseur" target="_blank">**Baptiste Vasseur**</a>
| :---: |:---:|:---:| :---:| :---:|
| <a href="https://gitlab.com/christophele"><img src="https://gitlab.com/uploads/-/system/user/avatar/1408322/avatar.png?width=200" width="200"/></a>  | <a href="https://gitlab.com/kiliandiogo"><img src="https://secure.gravatar.com/avatar/6e610014305ee861da9cb4bd84a6dbba?s=800&d=identicon" width="200"/></a> | <a href="https://gitlab.com/nolway"><img src="https://gitlab.com/uploads/-/system/user/avatar/1241492/avatar.png?width=400" width="200"/></a>  | <a href="https://gitlab.com/kamISKRANE"><img src="https://gitlab.com/uploads/-/system/user/avatar/3603263/avatar.png?width=400" width="200"/></a> | <a href="https://gitlab.com/BaptisteVasseur"><img src="https://gitlab.com/uploads/-/system/user/avatar/3786758/avatar.png?width=400" width="200"/></a>
